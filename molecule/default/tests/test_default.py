import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/tmp/deployment_artefacts",
    "/etc/ansible/facts.d",
    "/opt/coremedia/user-changes/server-lib",
    "/opt/coremedia/user-changes/common-lib",
    "/opt/coremedia/user-changes/current/webapps/user-changes/WEB-INF/properties/corem"
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/ansible/facts.d/user-changes.fact",
    "/opt/coremedia/user-changes/common-lib/coremedia-tomcat.jar",
    "/opt/coremedia/user-changes/current/bin/setenv.sh",
    "/opt/coremedia/user-changes/user-changes.properties"
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.user("coremedia").exists
    assert host.user("user-changes").exists
    assert host.group("coremedia").exists
